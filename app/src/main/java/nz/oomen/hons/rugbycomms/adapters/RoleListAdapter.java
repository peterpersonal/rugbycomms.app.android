package nz.oomen.hons.rugbycomms.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import nz.oomen.hons.rugbycomms.R;
import nz.oomen.hons.rugbycomms.models.Role;

/**
 * Created by Peter on 21/3/2016.
 */
public class RoleListAdapter extends ArrayAdapter<Role> {

    private Context mContext;

    public RoleListAdapter(Context context, List<Role> roles) {
        super(context, R.layout.role_list_item, roles);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.role_list_item, null);
        }

        // Get the Role for this item
        Role role = getItem(position);

        // Get the elements in the view we need to alter
        TextView view_roleName = (TextView)convertView.findViewById(R.id.textView_roleName);
        TextView view_roleInfo = (TextView)convertView.findViewById(R.id.textView_roleInfo);

        // Set the fields in the view to the values in the role object
        view_roleName.setText(role.getDisplayName());
        view_roleInfo.setText(role.getInfo());

        return convertView;
    }
}
