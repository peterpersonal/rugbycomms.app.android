package nz.oomen.hons.rugbycomms.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import nz.oomen.hons.rugbycomms.fragments.CallFragment;
import nz.oomen.hons.rugbycomms.fragments.MessagesFragment;

/**
 * Created by Peter on 17/5/2016.
 */
public class TabFragmentPagerAdapter extends FragmentPagerAdapter {
    private Fragment[] fragments = new Fragment[] {
        new CallFragment(),
        new MessagesFragment()
    };

    private String[] titles = new String[] {
        "Call", "Messages"
    };

    public TabFragmentPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }


    @Override
    public int getCount() {
        return fragments.length;
    }
}
