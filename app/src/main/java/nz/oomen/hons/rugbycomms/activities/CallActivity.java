package nz.oomen.hons.rugbycomms.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import nz.oomen.hons.rugbycomms.R;
import nz.oomen.hons.rugbycomms.adapters.TabFragmentPagerAdapter;
import nz.oomen.hons.rugbycomms.communication.SipConnection;
import nz.oomen.hons.rugbycomms.fragments.CallFragment;
import nz.oomen.hons.rugbycomms.fragments.MessagesFragment;

public class CallActivity extends FragmentActivity implements SipConnection.CallStatusListener {

    /**
     * Memebers
     */
    TextView mCallStatusView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        // Setup toolbar
        Toolbar tb = (Toolbar)findViewById(R.id.toolbar);
        tb.setTitle("Referee");
        tb.setTitleTextColor(ContextCompat.getColor(this, R.color.colorTextTitle));

        // Setup ViewPager
        ViewPager vp = (ViewPager)findViewById(R.id.call_pager);
        TabLayout tl = (TabLayout)findViewById(R.id.call_tabs);
        vp.setAdapter(new TabFragmentPagerAdapter(getFragmentManager()));
        tl.setupWithViewPager(vp);

        // Setup call status
        mCallStatusView = (TextView)findViewById(R.id.textView_callStatus);

        // Join the call
        SipConnection.INSTANCE.registerAsCallStatusListener(this);
        SipConnection.INSTANCE.joinCall();
    }

    @Override
    public void callStatusChanged(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCallStatusView.setText(status);
            }
        });
    }
}
