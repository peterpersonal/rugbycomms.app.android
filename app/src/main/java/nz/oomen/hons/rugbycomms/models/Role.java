package nz.oomen.hons.rugbycomms.models;

/**
 * Created by Peter on 21/3/2016.
 */
public class Role {

    // The name displayed for this role, in the role list
    private String displayName;
    public String getDisplayName() { return displayName; }

    // Information about this role, as displayed in the role list
    private String info;
    public String getInfo() { return info; }

    // The username for this role
    private String userName;
    public String getUserName() { return userName; }

    // The password for this role
    private String password;
    public String getPassword() { return password; }

    public Role(String displayName, String info, String userName, String password) {
        this.displayName = displayName;
        this.info = info;
        this.userName = userName;
        this.password = password;
    }
}
