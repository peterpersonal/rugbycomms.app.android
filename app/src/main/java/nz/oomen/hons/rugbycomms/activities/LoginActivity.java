package nz.oomen.hons.rugbycomms.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import nz.oomen.hons.rugbycomms.R;
import nz.oomen.hons.rugbycomms.adapters.RoleListAdapter;
import nz.oomen.hons.rugbycomms.communication.SipConnection;
import nz.oomen.hons.rugbycomms.models.Role;

public class LoginActivity extends AppCompatActivity {
    private static final int PERMISSIONS_REQUEST_USE_SIP = 0;
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private ListView mListView_roleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Setup the role list
        // Add some temp roles
        List<Role> roleList = new ArrayList<Role>();
        roleList.add(new Role(
                "Referee",
                "Actually just user1",
                "user1",
                "1"
        ));
        roleList.add(new Role(
                "Assistant Referee",
                "Actually just user2",
                "user2",
                "1"
        ));
        mListView_roleList = (ListView)findViewById(R.id.listView_roleList);
        RoleListAdapter roleListAdapter = new RoleListAdapter(this, roleList);
        mListView_roleList.setAdapter(roleListAdapter);
        mListView_roleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                on_roleListClick(parent, position);
            }
        });

        // Before we do much, we need to get the permission USE_SIP
        checkForPermissions();
    }
    // Event methods
    private void on_roleListClick(AdapterView<?> parent, int position) {
        Role role = (Role)parent.getItemAtPosition(position);

        // Execute the loginTask
        LoginTask loginTask = new LoginTask(role);
        loginTask.execute();
    }

    private void checkForPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.USE_SIP)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.USE_SIP},
                    PERMISSIONS_REQUEST_USE_SIP);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    PERMISSIONS_REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_USE_SIP: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Warning");
                    builder.setMessage("Application can't function without this permission. Please enable to use.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            System.exit(0);
                        }
                    });
                    AlertDialog diag = builder.create();
                    diag.show();
                }
                return;
            }
            case PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Warning");
                    builder.setMessage("Application can't function without this permission. Please enable to use.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            System.exit(0);
                        }
                    });
                    AlertDialog diag = builder.create();
                    diag.show();
                }
                return;
            }
        }
    }

    // Used to run the login task asynchronously
    private class LoginTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog mDialog = new ProgressDialog(LoginActivity.this);
        Role mRole;

        public LoginTask(Role role) {
            mRole = role;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog.setMessage("Authenticating...");
            mDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            SipConnection.INSTANCE.doAuthenticate(mRole, LoginActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mDialog.dismiss();

            // Launch the next activity
            startActivity(new Intent(LoginActivity.this, CallActivity.class));
        }
    }
}
