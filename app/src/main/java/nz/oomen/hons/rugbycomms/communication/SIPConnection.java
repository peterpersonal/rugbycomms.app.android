package nz.oomen.hons.rugbycomms.communication;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.sip.SipAudioCall;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;
import android.net.sip.SipRegistrationListener;
import android.util.Log;

import java.text.ParseException;

import nz.oomen.hons.rugbycomms.models.Role;

/**
 * Created by Peter on 21/3/2016.
 */
public class SipConnection implements SipRegistrationListener {
    /**
     * Constants
     */

    // The actual instance of this class, as per the singleton design paradigm
    public static final SipConnection INSTANCE = new SipConnection();

    public static final String SIP_DOMAIN = "192.168.20.22";  // IP of my Raspi SIP server
    public static final String CONFERENCE_URI = "3001@" + SIP_DOMAIN;
    public static final int EXPIRY_TIME = 60000;

    // Status strings
    public static final String STATUS_INITIAL = "Not logged in";
    public static final String STATUS_AUTHENTICATING = "Logging in...";
    public static final String STATUS_AUTHENTICATED= "Logged in";
    public static final String STATUS_AUTHENTICATION_FAILED = "Authentication failed";

    public static final String STATUS_NOT_IN_CALL = "Not in call";
    public static final String STATUS_JOINING_CALL = "Joining call...";
    public static final String STATUS_IN_CALL = "In call";

    /**
     * Members
     */

    // Current status
    private String authStatus = STATUS_INITIAL;
    public String getAuthStatus() { return authStatus; }

    private String callStatus = STATUS_NOT_IN_CALL;
    public String getCallStatus() { return callStatus; }

    // Call status listener
    private CallStatusListener mListener;
    public void registerAsCallStatusListener(CallStatusListener listener) {
        mListener = listener;
    }

    // SIP classes
    private SipManager mSipManager;
    private SipProfile mSipProfile;
    private SipAudioCall mSipCall;

    private Role mCurrentRole;

    // Private constructor, to enforce singleton design paradigm
    private SipConnection() {}

    public boolean doAuthenticate(Role role, Context context) {
        mCurrentRole = role;

        if(mSipManager == null) {
            mSipManager = SipManager.newInstance(context);
        }

        try {
            SipProfile.Builder builder = new SipProfile.Builder(mCurrentRole.getUserName(), SIP_DOMAIN);
            builder.setPassword(mCurrentRole.getPassword());
            mSipProfile = builder.build();

            Intent intent = new Intent();
            intent.setAction("android.SipDemo.INCOMING_CALL");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, Intent.FILL_IN_DATA);
            mSipManager.open(mSipProfile, pendingIntent, null);

            mSipManager.register(mSipProfile, EXPIRY_TIME, this);
        } catch(ParseException e) {
            Log.e("SIP-000", "Invalid username", e);
            return false;
        } catch (SipException e) {
            Log.e("SIP-001", "An exception was thrown on registration", e);
            return false;
        }
        return true;
    }

    /**
     * Join the confertence call
     * @return whether we were able to join the conference call or not
     */
    public boolean joinCall() {
        SipAudioCall.Listener listener = new SipAudioCall.Listener() {
            @Override
            public void onCallEstablished(SipAudioCall call) {
                mSipCall.startAudio();
                mSipCall.setSpeakerMode(true);
                callStatus = STATUS_IN_CALL;
                mListener.callStatusChanged(callStatus);
            }

            @Override
            public void onCallEnded(SipAudioCall call) {
                callStatus = STATUS_NOT_IN_CALL;
                mListener.callStatusChanged(callStatus);
            }
        };
        try {
            mSipCall = mSipManager.makeAudioCall(mSipProfile.getUriString(), CONFERENCE_URI, listener, 0);
        } catch (SipException e) {
            Log.e("SIP-003", "An exception was thrown trying to place call", e);
            if (mSipCall != null) {
                mSipCall.close();
            }
            return false;
        }
        callStatus = STATUS_JOINING_CALL;
        mListener.callStatusChanged(callStatus);
        return true;
    }

    @Override
    public void onRegistering(String localProfileUri) {
        authStatus = STATUS_AUTHENTICATING;
    }

    @Override
    public void onRegistrationDone(String localProfileUri, long expiryTime) {
        authStatus = STATUS_AUTHENTICATED;
    }

    @Override
    public void onRegistrationFailed(String localProfileUri, int errorCode, String errorMessage) {
        authStatus = STATUS_AUTHENTICATION_FAILED;
    }

    public interface CallStatusListener {
        void callStatusChanged(String status);
    }
}
